// Copyright Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "Player/ShooterCharacterMovement.h"


DEFINE_LOG_CATEGORY_STATIC(LogShooterCharacterMovement, Log, All);

#pragma region SavedMove

uint8 FMBSavedMove_ExtCharacter::GetCompressedFlags() const
{
	uint8 Result = FSavedMove_Character::GetCompressedFlags();

	/**
	* Add the Custom_0 flag to represent the Teleport flag in the Compressed Flags integer value;
	* this way there's no need for further changes in the replication system, as the Teleport flag
	* will be replicated together with the original ones.
	* */
	if (bPressedTeleport)
	{
		Result |= FLAG_Custom_0;
	}

	return Result;
}


void FMBSavedMove_ExtCharacter::SetInitialPosition(ACharacter* C)
{
	FSavedMove_Character::SetInitialPosition(C);

	// Set the Teleport flag on Move initialization
	bPressedTeleport = Cast<AShooterCharacter>(C)->bPressedTeleport;
}

#pragma endregion

#pragma region NetworkPredicitonData

FSavedMovePtr FMBNetworkPredictionData_Client_ExtCharacter::AllocateNewMove()
{
	// Override needed to allocate the Extended version of the Character Moves instead of the original type
	return FSavedMovePtr(new FMBSavedMove_ExtCharacter());
}

#pragma endregion


#pragma region Shooter Character Movement

//----------------------------------------------------------------------//
// UPawnMovementComponent
//----------------------------------------------------------------------//
UShooterCharacterMovement::UShooterCharacterMovement(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	TeleportDistance = 1000.0f;
	TeleportMaxSweepChecks = 25;
	TeleportCapsuleSweepScaleFactor = 0.5f;
	
	WallJumpSweepOuterShell = 10.0f;
	WallJumpMinNormalPushSpeed = 500.0f;
	WallJumpMinVerticalPushSpeed = 750.0f;
}


float UShooterCharacterMovement::GetMaxSpeed() const
{
	float MaxSpeed = Super::GetMaxSpeed();

	const AShooterCharacter* ShooterCharacterOwner = Cast<AShooterCharacter>(PawnOwner);
	if (ShooterCharacterOwner)
	{
		if (ShooterCharacterOwner->IsTargeting())
		{
			MaxSpeed *= ShooterCharacterOwner->GetTargetingSpeedModifier();
		}
		if (ShooterCharacterOwner->IsRunning())
		{
			MaxSpeed *= ShooterCharacterOwner->GetRunningSpeedModifier();
		}
	}

	return MaxSpeed;
}



#pragma region EXTENSION

FNetworkPredictionData_Client* UShooterCharacterMovement::GetPredictionData_Client() const
{
	// Replace the original logic in order to change the ClientPredictionData class
	if (ClientPredictionData == nullptr)
	{
		UShooterCharacterMovement* MutableThis = const_cast<UShooterCharacterMovement*>(this);
		MutableThis->ClientPredictionData = new FMBNetworkPredictionData_Client_ExtCharacter(*this);
	}

	return ClientPredictionData;
}


void UShooterCharacterMovement::SetUpdatedComponent(USceneComponent* NewUpdatedComponent)
{
	// Just check that the Owner is an Extended Character (in addition to normal Character checks)
	if (NewUpdatedComponent)
	{
		const AShooterCharacter* NewCharacterOwner = Cast<AShooterCharacter>(NewUpdatedComponent->GetOwner());
		if (NewCharacterOwner == NULL)
		{
			UE_LOG(LogShooterCharacterMovement, Error, TEXT("%s owned by %s must update a component owned by a ShooterCharacter"), *GetName(), *GetNameSafe(NewUpdatedComponent->GetOwner()));
			return;
		}
	}

	Super::SetUpdatedComponent(NewUpdatedComponent);
}

void UShooterCharacterMovement::UpdateFromCompressedFlags(uint8 Flags)
{
	Super::UpdateFromCompressedFlags(Flags);

	// No need to continue if the Owner is not an Extended Character
	AShooterCharacter* ShooterCharacterOwner = Cast<AShooterCharacter>(CharacterOwner);

	if (ShooterCharacterOwner)
	{
		// Store the old Teleport flag to check for differences
		bool bHadPressedTeleport = ShooterCharacterOwner->bPressedTeleport;

		// Unpack the Teleport flag from Compressed Flags
		ShooterCharacterOwner->bPressedTeleport = ((Flags & FSavedMove_Character::FLAG_Custom_0) != 0);

		// On the Server, replicate the TeleportForward move if it hadn't already been replicated
		if (ShooterCharacterOwner->HasAuthority())
		{
			if (ShooterCharacterOwner->bPressedTeleport && !bHadPressedTeleport)
			{
				ShooterCharacterOwner->TeleportForward();
			}
		}
	}

}

void UShooterCharacterMovement::OnMovementUpdated(float DeltaSeconds, const FVector& OldLocation, const FVector& OldVelocity)
{
	// Actually Execute the Teleport if the Owner is an Shooter Character and the Teleport flag is set
	AShooterCharacter* ShooterCharacterOwner = Cast<AShooterCharacter>(CharacterOwner);
	if (ShooterCharacterOwner)
	{
		ShooterCharacterOwner->CheckTeleportInput(DeltaSeconds);
	}
}


#pragma region Teleport

bool UShooterCharacterMovement::DoTeleportForward(bool bReplayingMoves)
{
	if (CharacterOwner)
	{
		// Prepare CollisionObjectQueryParams (check for WorldStatic, PhysicsBody and Pawn objects)
		// Could be cached, but keep open for changes in detection at runtime (e.g. ghostly powerup that allows to pass through WorldStatic objects)
		FCollisionObjectQueryParams Params = FCollisionObjectQueryParams();
		Params.AddObjectTypesToQuery(ECollisionChannel::ECC_WorldStatic);
		Params.AddObjectTypesToQuery(ECollisionChannel::ECC_Pawn);
		Params.AddObjectTypesToQuery(ECollisionChannel::ECC_PhysicsBody);

		// Get the Owner's Collision Shape and scale it according to its Scale Factor
		FCollisionShape Shape = CharacterOwner->GetCapsuleComponent()->GetCollisionShape();
		Shape.SetCapsule(TeleportCapsuleSweepScaleFactor * Shape.Capsule.Radius, TeleportCapsuleSweepScaleFactor * Shape.Capsule.HalfHeight);

		const FVector StartingLoc = CharacterOwner->GetActorLocation();
		const FVector ForwardVec = CharacterOwner->GetActorForwardVector();

		/** On each step, the distance is decremented by a constant amount; the granularity of the process is determined by the MaxSweepChecks
		*   property of the Owner
		*/
		const float DistanceDecrement = TeleportDistance / TeleportMaxSweepChecks;

		FVector Delta;

		// Keep checking till the checked distance is zero (i.e. after MaxSweepChecks steps)
		for (float CurrentDistance = TeleportDistance; CurrentDistance > 0; CurrentDistance -= DistanceDecrement)
		{
			Delta = ForwardVec * CurrentDistance;

			// No overlap ---> valid destination
			if (!GetWorld()->OverlapAnyTestByObjectType(StartingLoc + Delta, UpdatedComponent->GetComponentRotation().Quaternion(), Params, Shape))
			{
				/**
				* Use this method to correct partial compenetrations; no need to sweep as the destination has already been validated.
				* Moreover, sweeping would not allow blinking over other obstacles in the middle!
				*/
				FHitResult Hit;
				SafeMoveUpdatedComponent(Delta, CharacterOwner->GetActorRotation(), false, Hit, ETeleportType::TeleportPhysics);
				return true;
			}
		}
	}
	
	return false;
}


#pragma endregion

#pragma region Wall Jump

bool UShooterCharacterMovement::CanAttemptWallJump() const
{
	return IsJumpAllowed() &&
		!bWantsToCrouch &&
		IsFalling(); // If the character is on ground, they should just jump normally
}


bool UShooterCharacterMovement::DoWallJump(bool bReplayingMoves)
{
	AShooterCharacter* ShooterOwner = Cast<AShooterCharacter>(CharacterOwner);

	if (ShooterOwner && ShooterOwner->CanWallJump())
	{
		// Add the outer shell to the Shape and halve it's height; we will check only the lower half of the capsule,
		// considering the Character should use their legs to push themselves on the wall!
		FCollisionShape Shape = CharacterOwner->GetCapsuleComponent()->GetCollisionShape();
		Shape.SetCapsule(Shape.Capsule.Radius + WallJumpSweepOuterShell, Shape.Capsule.HalfHeight / 2);

		FVector Loc = CharacterOwner->GetActorLocation() + FVector::DownVector * Shape.Capsule.HalfHeight / 2;
		
		FHitResult Hit;
		if (GetWorld()->SweepSingleByChannel(Hit, Loc, Loc, FQuat::Identity, WallChannel, Shape))
		{
			// Make sure the Hit represents an actual wall, i.e. a non-walkable slope
			if (Hit.ImpactNormal.Z < GetWalkableFloorZ())
			{
				float NormalComp = FVector::DotProduct(Velocity, Hit.ImpactNormal);
				FVector ReflectedVelocity = Velocity - 2 * NormalComp * Hit.ImpactNormal;
				
				float NormalCompDiff = WallJumpMinNormalPushSpeed - FMath::Abs(NormalComp);
				if (NormalCompDiff > 0)
				{
					ReflectedVelocity += NormalCompDiff * Hit.ImpactNormal;
				}

				// If the Character can't move up or down, null out the vertical velocity, consider planar movement only
				if (!bConstrainToPlane || FMath::Abs(PlaneConstraintNormal.Z) != 1.f)
				{
					ReflectedVelocity.Z = FMath::Max(ReflectedVelocity.Z, WallJumpMinVerticalPushSpeed);
				}
				else
				{
					ReflectedVelocity.Z = 0;
				}

				Velocity = ReflectedVelocity;
				SetMovementMode(MOVE_Falling);
				return true;
			}
		}
	}
	
	return false;
}

#pragma endregion

#pragma endregion
