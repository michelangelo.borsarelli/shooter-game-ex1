// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "Pickups/MBShooterPickup_Weapon.h"

#pragma region Actor callbacks

AMBShooterPickup_Weapon::AMBShooterPickup_Weapon(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// Add a Root for all Mesh related components
	MeshRoot = CreateDefaultSubobject<USceneComponent>(TEXT("MeshRoot"));
	MeshRoot->SetupAttachment(RootComponent);

	// Add Skeletal Mesh component to represent the Weapon
	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	WeaponMesh->SetHiddenInGame(true);
	WeaponMesh->SetupAttachment(MeshRoot);
	
	// Actor should tick to rotate the Mesh
	PrimaryActorTick.bCanEverTick = true;

	// Replicate Movement so that the Pickup can be easily repositioned on Clients
	SetReplicateMovement(true);
	
	RotationSpeed = 60.0f;
}

void AMBShooterPickup_Weapon::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	MeshRoot->AddRelativeRotation(FQuat::MakeFromEuler(FVector::UpVector * RotationSpeed * DeltaSeconds));
}

void AMBShooterPickup_Weapon::BeginPlay()
{
	Super::BeginPlay();

	// If graphic data is available, update graphics (generally called for Client replication)
	if (GraphicData.Mesh->IsValidLowLevel())
	{
		UpdateGraphics();
	}
}

void AMBShooterPickup_Weapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AMBShooterPickup_Weapon, GraphicData);
}

#pragma endregion

#pragma region Pickup overrides

void AMBShooterPickup_Weapon::GivePickupTo(class AShooterCharacter* Pawn)
{
	Super::GivePickupTo(Pawn);
	
	// Only change Character's inventory on the Server
	if (HasAuthority() && Pawn && WeaponClass->IsValidLowLevel())
	{
		// Check if the Character already has the contained Weapon
		AShooterWeapon* OwnedWeapon = (Pawn ? Pawn->FindWeapon(WeaponClass) : NULL);

		if (OwnedWeapon)
		{
			// In that case, just add ammo
			OwnedWeapon->GiveAmmo(Ammo);
		}
		else
		{
			// If the Character doesn't own the contained Weapon, create a new one with the specified ammo count
			FActorSpawnParameters SpawnInfo;
			SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			AShooterWeapon* NewWeapon = GetWorld()->SpawnActor<AShooterWeapon>(WeaponClass, SpawnInfo);
			NewWeapon->SetCurrentAmmo(Ammo);

			AShooterWeapon* OldWeapon = Pawn->GetWeapon();
			
			// First off add the Weapon and Equip it, then remove old Weapon when it is not relevant anymore
			Pawn->AddWeapon(NewWeapon);
			Pawn->EquipWeapon(NewWeapon);
			Pawn->RemoveWeapon(OldWeapon);
		}
	}
}

void AMBShooterPickup_Weapon::OnPickedUp()
{
	WeaponMesh->SetHiddenInGame(true);
	Super::OnPickedUp();
}

void AMBShooterPickup_Weapon::OnRespawned()
{
	WeaponMesh->SetHiddenInGame(false);
	Super::OnRespawned();
}

void AMBShooterPickup_Weapon::OnExpired()
{
	WeaponMesh->SetHiddenInGame(true);
	Super::OnExpired();
}

bool AMBShooterPickup_Weapon::CanBePickedUp(AShooterCharacter* TestPawn) const
{
	// Can't be picked up if the Weapon class is not valid!
	if (!WeaponClass->IsValidLowLevel())
	{
		return false;
	}

	// If the Player already owns the Weapon, they can pick this up only if they have less than max ammo (it would be useless otherwise)
	
	if (IsActive())
	{
		AShooterWeapon* TestWeapon = (TestPawn ? TestPawn->FindWeapon(WeaponClass) : NULL);
		if (TestWeapon)
		{
			return TestWeapon->GetCurrentAmmo() < TestWeapon->GetMaxAmmo();
		}
		else
		{
			// avoid unwanted clashing with a pending equip
			return TestPawn->GetWeapon()->IsEquipped();
		}
	}

	return true;
}

#pragma endregion

#pragma region Weapon Pickup specific

void AMBShooterPickup_Weapon::SetupFromWeapon(AShooterWeapon* SourceWeapon)
{
	// Copy Weapon Class and Ammo count
	WeaponClass = SourceWeapon->GetClass();
	Ammo = SourceWeapon->GetCurrentAmmo();
	
	// Update Graphic Data for replication
	GraphicData.Mesh = SourceWeapon->GetWeaponMesh()->SkeletalMesh;
	GraphicData.Materials = SourceWeapon->GetWeaponMesh()->GetMaterials();	

	UpdateGraphics();
}

void AMBShooterPickup_Weapon::UpdateGraphics()
{
	// Update Skeletal Mesh and assigned Materials
	WeaponMesh->SetSkeletalMesh(GraphicData.Mesh);

	for (int i = 0; i < GraphicData.Materials.Num(); i++)
	{
		WeaponMesh->SetMaterial(i, GraphicData.Materials[i]);
	}
}

void AMBShooterPickup_Weapon::OnRep_GraphicData()
{
	UpdateGraphics();
}

#pragma endregion