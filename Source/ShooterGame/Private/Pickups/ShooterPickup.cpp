// Copyright Epic Games, Inc. All Rights Reserved.

#include "ShooterGame.h"
#include "Pickups/ShooterPickup.h"
#include "Particles/ParticleSystemComponent.h"

AShooterPickup::AShooterPickup(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	UCapsuleComponent* CollisionComp = ObjectInitializer.CreateDefaultSubobject<UCapsuleComponent>(this, TEXT("CollisionComp"));
	CollisionComp->InitCapsuleSize(40.0f, 50.0f);
	CollisionComp->SetCollisionObjectType(COLLISION_PICKUP);
	CollisionComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	CollisionComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	RootComponent = CollisionComp;

	PickupPSC = ObjectInitializer.CreateDefaultSubobject<UParticleSystemComponent>(this, TEXT("PickupFX"));
	PickupPSC->bAutoActivate = false;
	PickupPSC->bAutoDestroy = false;
	PickupPSC->SetupAttachment(RootComponent);

	RespawnTime = 10.0f;
	State = EPickupState::Active;
	PickedUpBy = NULL;

	bShouldRespawn = true;
	bHasLifetime = false;
	Lifetime = 10.0f;

	SetRemoteRoleForBackwardsCompat(ROLE_SimulatedProxy);
	bReplicates = true;
}

void AShooterPickup::BeginPlay()
{
	Super::BeginPlay();

	// register on pickup list (server only), don't care about unregistering (in FinishDestroy) - no streaming
	AShooterGameMode* GameMode = GetWorld()->GetAuthGameMode<AShooterGameMode>();
	if (GameMode)
	{
		GameMode->LevelPickups.Add(this);

		// On Server always respawn it on BeginPlay, as we have authority
		RespawnPickup();
	}
	else if (State == EPickupState::Active)
	{
		// On the client instead use the replicated State, as a Pickup may be picked up as soon as it is spawned on the Server,
		// creating a ghost pickup on the Client otherwise
		RespawnPickup();
	}
}

void AShooterPickup::NotifyActorBeginOverlap(class AActor* Other)
{
	Super::NotifyActorBeginOverlap(Other);
	PickupOnTouch(Cast<AShooterCharacter>(Other));
}

bool AShooterPickup::CanBeActive() const
{
	// Either it is Active already or it is going to respawn by itself at some point
	return State == EPickupState::Active || bShouldRespawn;
}

bool AShooterPickup::CanBePickedUp(class AShooterCharacter* TestPawn) const
{
	return TestPawn && TestPawn->IsAlive();
}

void AShooterPickup::GivePickupTo(class AShooterCharacter* Pawn)
{
}

void AShooterPickup::PickupOnTouch(class AShooterCharacter* Pawn)
{
	if (IsActive() && Pawn && Pawn->IsAlive() && !IsPendingKill())
	{
		if (CanBePickedUp(Pawn))
		{
			GivePickupTo(Pawn);
			PickedUpBy = Pawn;

			if (!IsPendingKill())
			{
				State = EPickupState::PickedUp;
				OnPickedUp();

				if (bShouldRespawn && RespawnTime > 0.0f)
				{
					GetWorldTimerManager().SetTimer(TimerHandle_RespawnPickup, this, &AShooterPickup::RespawnPickup, RespawnTime, false);
				}
			}
		}
	}
}

void AShooterPickup::RespawnPickup()
{
	// Stop any Timer, effectively resetting the Pickup to its Active state
	if (TimerHandle_RespawnPickup.IsValid())
	{
		GetWorldTimerManager().ClearTimer(TimerHandle_RespawnPickup);
	}

	if (TimerHandle_Lifetime.IsValid())
	{
		GetWorldTimerManager().ClearTimer(TimerHandle_Lifetime);
	}

	State = EPickupState::Active;
	PickedUpBy = NULL;
	OnRespawned();

	TSet<AActor*> OverlappingPawns;
	GetOverlappingActors(OverlappingPawns, AShooterCharacter::StaticClass());

	for (AActor* OverlappingPawn : OverlappingPawns)
	{
		PickupOnTouch(CastChecked<AShooterCharacter>(OverlappingPawn));	
	}

	// If the Pickup is still Active (i.e. wasn't overlapping any Character on respawn) and has a lifetime, set the lifetime timer
	if (IsActive() && bHasLifetime && Lifetime > 0.0f)
	{
		GetWorldTimerManager().SetTimer(TimerHandle_Lifetime, this, &AShooterPickup::Expire, Lifetime);
	}
}

void AShooterPickup::OnPickedUp()
{
	if (RespawningFX)
	{
		PickupPSC->SetTemplate(RespawningFX);
		PickupPSC->ActivateSystem();
	}
	else
	{
		PickupPSC->DeactivateSystem();
	}

	if (PickupSound && PickedUpBy)
	{
		UGameplayStatics::SpawnSoundAttached(PickupSound, PickedUpBy->GetRootComponent());
	}

	OnPickedUpEvent();
}

void AShooterPickup::OnRespawned()
{
	if (ActiveFX)
	{
		PickupPSC->SetTemplate(ActiveFX);
		PickupPSC->ActivateSystem();
	}
	else
	{
		PickupPSC->DeactivateSystem();
	}

	const bool bJustSpawned = CreationTime <= (GetWorld()->GetTimeSeconds() + 5.0f);
	if (RespawnSound && !bJustSpawned)
	{
		UGameplayStatics::PlaySoundAtLocation(this, RespawnSound, GetActorLocation());
	}

	OnRespawnEvent();
}

// Replaces OnRep_IsActive()
void AShooterPickup::OnRep_State()
{
	switch (State)
	{
		case EPickupState::Active:
			OnRespawned();
			break;

		case EPickupState::PickedUp:
			OnPickedUp();
			break;

		case EPickupState::Expired:
			OnExpired();
			break;

		default:
			break;
	}
}

void AShooterPickup::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AShooterPickup, PickedUpBy);	
	DOREPLIFETIME(AShooterPickup, State);
}

void AShooterPickup::Expire()
{
	// Set Expired State
	State = EPickupState::Expired;

	// Play OnExpired effects
	OnExpired();
	
	// Start the respawn timer if it should Respawn
	if (bShouldRespawn && RespawnTime > 0.0f)
	{
		GetWorldTimerManager().SetTimer(TimerHandle_RespawnPickup, this, &AShooterPickup::RespawnPickup, RespawnTime, false);
	}
}

void AShooterPickup::OnExpired()
{
	// Just deactivate FX when expired
	PickupPSC->DeactivateSystem();

	// Call blueprint event
	OnExpiredEvent();
}