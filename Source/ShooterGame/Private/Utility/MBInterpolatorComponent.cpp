// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "Utility/MBInterpolatorComponent.h"

#pragma region GENERIC INTERPOLATION

bool FMBInterpolation::TickInterpolation(float DeltaSeconds)
{
	// Update CurrentTime and call OnTick
	CurrentTime = FMath::Min(Duration, CurrentTime + DeltaSeconds);
	return OnTick(DeltaSeconds);
}

#pragma endregion

#pragma region SCALE FACTOR INTERPOLATION

bool FMBScaleFactorInterpolation::OnTick(float DeltaSeconds)
{
	if (!TargetCharacter->IsValidLowLevel()) return false;

	// Use Curve is not null, otherwise just lerp over time
	if (Curve)
	{
		TargetCharacter->SetScaleFactor(StartVal + (EndVal - StartVal) * (bInverseCurve ? 1-Curve->GetFloatValue(1-GetPercent()) : Curve->GetFloatValue(GetPercent())));
	}
	else
	{
		TargetCharacter->SetScaleFactor(FMath::Lerp(StartVal, EndVal, GetPercent()));
	}

	return true;
}

#pragma endregion

#pragma region INTERPOLATOR

UMBInterpolatorComponent::UMBInterpolatorComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}


void UMBInterpolatorComponent::InterpolateScaleFactor(float InStartVal, float InEndVal, float InDurationSeconds, UCurveFloat* InCurve, bool bUseInverseCurve, bool bForceStartVal, bool bAdjustTime)
{
	// Forward to Server if called on Client
	if (!GetOwner()->HasAuthority())
	{
		Server_InterpolateScaleFactor(InStartVal, InEndVal, InDurationSeconds, InCurve, bForceStartVal, bAdjustTime);
		return;
	}

	// Make sure the Owner is actually a ShooterCharacter, thus supporting ScaleFactor interpolation
	AShooterCharacter* OwnerCharacter = Cast<AShooterCharacter>(GetOwner());

	if (OwnerCharacter)
	{
		FMBScaleFactorInterpolation Interp = FMBScaleFactorInterpolation();

		// Make sure the interpolation points to the correct Character 
		Interp.TargetCharacter = OwnerCharacter;

		Interp.Curve = InCurve;
		Interp.bInverseCurve = bUseInverseCurve;

		Interp.StartTimestamp = GetWorld()->GetGameState()->GetServerWorldTimeSeconds();
		Interp.EndVal = InEndVal;
		Interp.CurrentTime = 0;

		// If not forcing the starting value use the current ScaleFactor as StartVal
		Interp.StartVal = bForceStartVal ? InStartVal : OwnerCharacter->GetScaleFactor();

		// If the StartVal was not forced and duration should be adjusted, recalculate duration based on absolute diffs between starting and ending values
		Interp.Duration = !bForceStartVal && bAdjustTime ?
				InDurationSeconds * FMath::Abs(InEndVal - OwnerCharacter->GetScaleFactor()) / FMath::Abs(InEndVal - InStartVal)
				: InDurationSeconds;

		ScaleFactorInterpolation = Interp;
	}
}

void UMBInterpolatorComponent::Server_InterpolateScaleFactor_Implementation(float InStartVal, float InEndVal, float InTimeSeconds, UCurveFloat* InCurve, bool bForceStartVal, bool bAdjustTime)
{
	InterpolateScaleFactor(InStartVal, InEndVal, InTimeSeconds, InCurve, bForceStartVal, bAdjustTime);
}


void UMBInterpolatorComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// Tick interpolation if not complete
	if (!ScaleFactorInterpolation.IsComplete())
	{
		ScaleFactorInterpolation.TickInterpolation(DeltaTime);
	}
}

#pragma endregion

