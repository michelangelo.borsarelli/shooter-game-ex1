// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "UI/UserWidgets/MBStatusEffectsList.h"
#include "Blueprint/WidgetTree.h"
#include "Components/VerticalBoxSlot.h"


void UMBStatusEffectsList::NativeConstruct()
{
	Super::NativeConstruct();

	AShooterPlayerController* PC = Cast<AShooterPlayerController>(GetWorld()->GetFirstPlayerController());

	if (PC)
	{
		// Bind to the Pawn Change delegate, then execute the callback on its current Pawn
		PC->OnPawnChange.AddDynamic(this, &UMBStatusEffectsList::OnPawnChange);
		OnPawnChange(PC->GetPawn());
	}
}

void UMBStatusEffectsList::OnPawnChange(APawn* NewPawn)
{
	// Unbind from the previous Effect Handler, if present
	if (TrackedEffectHandler->IsValidLowLevel())
	{
		TrackedEffectHandler->OnStatusEffectApplied.RemoveDynamic(this, &UMBStatusEffectsList::OnStatusEffectApplied);
		TrackedEffectHandler->OnStatusEffectRemoved.RemoveDynamic(this, &UMBStatusEffectsList::OnStatusEffectRemoved);
	}

	// If the PlayerController has possesed a new Pawn, get its Effect Handler and bind delegates
	if (NewPawn)
	{
		TrackedEffectHandler = Cast<UMBStatusEffectHandlerComponent>(NewPawn->GetComponentByClass(UMBStatusEffectHandlerComponent::StaticClass()));

		if (TrackedEffectHandler)
		{
			TrackedEffectHandler->OnStatusEffectApplied.AddDynamic(this, &UMBStatusEffectsList::OnStatusEffectApplied);
			TrackedEffectHandler->OnStatusEffectRemoved.AddDynamic(this, &UMBStatusEffectsList::OnStatusEffectRemoved);
		}
	}
}

void UMBStatusEffectsList::OnStatusEffectApplied(uint8 EffectId)
{
	// Get an available Tracker
	UMBStatusEffectTracker* Tracker = GetTrackerInstance();
	
	// Move the Tracker to the bottom of the box
	// MainBox->ShiftChild(MainBox->GetChildrenCount() - 1, Tracker); <--- unfortunately it can't be used in a packaged game
	MainBox->RemoveChild(Tracker);

	UVerticalBoxSlot* BoxSlot = MainBox->AddChildToVerticalBox(Tracker);
	BoxSlot->SetHorizontalAlignment(EHorizontalAlignment::HAlign_Fill);
	BoxSlot->SetVerticalAlignment(EVerticalAlignment::VAlign_Center);
	BoxSlot->SetPadding(FMargin(0.0f, 0.0f, 0.0f, Spacing));

	
	Tracker->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
	Tracker->Setup(TrackedEffectHandler, EffectId);
	Tracker->Show();
}

void UMBStatusEffectsList::OnStatusEffectRemoved(uint8 EffectId)
{
	// When a Status Effect is removed, search for the corresponding Tracker and Collapse it
	UMBStatusEffectTracker* Current;
	for (UWidget* Child : MainBox->GetAllChildren())
	{
		Current = Cast<UMBStatusEffectTracker>(Child);
		if (Current->GetStatusEffectId() == EffectId)
		{
			Current->Collapse();
			return;
		}
	}
}

UMBStatusEffectTracker* UMBStatusEffectsList::GetTrackerInstance()
{
	// Search MainBox's children for a collapsed one
	for (UWidget* Child : MainBox->GetAllChildren())
	{
		if (Child->GetVisibility() == ESlateVisibility::Collapsed)
		{
			return Cast<UMBStatusEffectTracker>(Child);
		}
	}

	// If didn't find one, create a new one
	UMBStatusEffectTracker* NewTracker = WidgetTree->ConstructWidget<UMBStatusEffectTracker>(TrackerClass);
	
	/*UVerticalBoxSlot* BoxSlot = MainBox->AddChildToVerticalBox(NewTracker);
	BoxSlot->SetHorizontalAlignment(EHorizontalAlignment::HAlign_Fill);
	BoxSlot->SetVerticalAlignment(EVerticalAlignment::VAlign_Center);
	BoxSlot->SetPadding(FMargin(0.0f, 0.0f, 0.0f, Spacing));*/
	
	return NewTracker;
}