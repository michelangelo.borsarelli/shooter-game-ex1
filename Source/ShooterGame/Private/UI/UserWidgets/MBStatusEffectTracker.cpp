#include "ShooterGame.h"
#include "UI/UserWidgets/MBStatusEffectTracker.h"

void UMBStatusEffectTracker::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	// If the Effect exists, update according to it, otherwise force zero values
	if (ReferenceHandler && ReferenceHandler->GetStatusEffect(StatusEffectId, StatusEffectInfo))
	{
		UpdateFromStatusEffectInfo();
	}
	else
	{
		SetRemainingTimeText(0.0f);
		RemainingTimeBar->SetPercent(0.0f);
	}
}

void UMBStatusEffectTracker::Show_Implementation()
{
	// By default, just make the Widget visible but not hit-testable
	SetVisibility(ESlateVisibility::SelfHitTestInvisible);
}

void UMBStatusEffectTracker::Collapse_Implementation()
{
	// By default, just make the Widget Collapsed
	SetVisibility(ESlateVisibility::Collapsed);
}

void UMBStatusEffectTracker::Setup(UMBStatusEffectHandlerComponent* InHandler, uint8 InEffectId)
{
	ReferenceHandler = InHandler;
	StatusEffectId = InEffectId;

	// Set Icon and execute the first update
	if (InHandler->GetStatusEffect(InEffectId, StatusEffectInfo))
	{
		SetStatusEffectIcon(StatusEffectInfo.Source->Icon);
		UpdateFromStatusEffectInfo();
	}
}

void UMBStatusEffectTracker::UpdateFromStatusEffectInfo_Implementation()
{
	float RemainingTime = StatusEffectInfo.GetRemainingTimeSeconds(this);

	// Update Text
	SetRemainingTimeText(RemainingTime);

	// Update Progress Bar
	if (RemainingTimeBar)
	{
		RemainingTimeBar->SetPercent(RemainingTime / StatusEffectInfo.GetDuration());
	}
}

void UMBStatusEffectTracker::SetRemainingTimeText(float RemainingTime)
{
	if (RemainingTimeText)
	{
		FNumberFormattingOptions NumberFormatOptions;
		NumberFormatOptions.MinimumFractionalDigits = TextPrecision;
		NumberFormatOptions.MaximumFractionalDigits = TextPrecision;

		RemainingTimeText->SetText(FText::AsNumber(RemainingTime, &NumberFormatOptions));
	}
}