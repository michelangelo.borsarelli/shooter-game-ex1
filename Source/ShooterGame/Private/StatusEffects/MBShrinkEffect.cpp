// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "Utility/MBInterpolatorComponent.h"
#include "StatusEffects/MBShrinkEffect.h"

void UMBShrinkEffect::OnApplied_Implementation(AShooterCharacter* AffectedCharacter)
{
	UMBInterpolatorComponent* Interpolator = Cast<UMBInterpolatorComponent>(AffectedCharacter->GetComponentByClass(UMBInterpolatorComponent::StaticClass()));
	if (Interpolator)
	{
		// If interpolated, adjust duration in advance, as it is needed for the delayed delegate binding
		float AdjustedTimeSeconds = InterpolationTimeSeconds * FMath::Abs(TargetScaleFactor - AffectedCharacter->GetScaleFactor()) / FMath::Abs(TargetScaleFactor - AffectedCharacter->DefaultScaleFactor);
		
		// Start interpolation
		Interpolator->InterpolateScaleFactor(
			AffectedCharacter->GetScaleFactor(),
			TargetScaleFactor,
			AdjustedTimeSeconds,
			InterpolationCurve,
			false,
			false,
			false
		);

		// Actually bind the Hit delegate when the interpolation is complete
		FTimerHandle Handle;
		FTimerDelegate BindingDelegate;
		BindingDelegate.BindLambda([this, AffectedCharacter]()
			{
				if (AffectedCharacter->IsValidLowLevel() && AffectedCharacter->IsAlive())
				{
					AffectedCharacter->GetCapsuleComponent()->OnComponentHit.AddDynamic(this, &UMBShrinkEffect::OnCharacterHit);
				}
			});

		GetWorld()->GetTimerManager().SetTimer(Handle, BindingDelegate, AdjustedTimeSeconds, false);
	}
	else
	{
		// If there is no interpolator, just set the scale and bind the delegate
		AffectedCharacter->SetScaleFactor(TargetScaleFactor);
		AffectedCharacter->GetCapsuleComponent()->OnComponentHit.AddDynamic(this, &UMBShrinkEffect::OnCharacterHit);
	}
}

void UMBShrinkEffect::OnRemoved_Implementation(AShooterCharacter* AffectedCharacter)
{
	
	UMBInterpolatorComponent* Interpolator = Cast<UMBInterpolatorComponent>(AffectedCharacter->GetComponentByClass(UMBInterpolatorComponent::StaticClass()));
	
	if (Interpolator)
	{
		float AdjustedTimeSeconds = InterpolationTimeSeconds * FMath::Abs(AffectedCharacter->DefaultScaleFactor - AffectedCharacter->GetScaleFactor()) / FMath::Abs(TargetScaleFactor - AffectedCharacter->DefaultScaleFactor);
		
		// Interpolate to the DefaultScaleFactor of the affected Character
		Interpolator->InterpolateScaleFactor(
			AffectedCharacter->GetScaleFactor(),
			AffectedCharacter->DefaultScaleFactor,
			AdjustedTimeSeconds,
			InterpolationCurve,
			true,
			false,
			false
		);
	}
	else
	{ 
		AffectedCharacter->SetScaleFactor(AffectedCharacter->DefaultScaleFactor);
	}
	// As soon as the effect is removed, interpolated or not, unbind the delegate
	AffectedCharacter->GetCapsuleComponent()->OnComponentHit.RemoveDynamic(this, &UMBShrinkEffect::OnCharacterHit);
}

void UMBShrinkEffect::OnCharacterHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	// Accept the Hit only if another Character was Hit and he was moving (it would make no sense being stomped by a still Character)
	AShooterCharacter* OtherCharacter = Cast<AShooterCharacter>(OtherActor);
	if (OtherCharacter && OtherCharacter->GetVelocity() != FVector::ZeroVector)
	{
		AShooterCharacter* AffectedCharacter = Cast<AShooterCharacter>(HitComponent->GetOwner());

		// Other Character must be bigger than Affected Character (positive difference and more than MinScaleDifference)
		if (OtherCharacter->GetScaleFactor() - AffectedCharacter->GetScaleFactor() >= MinScaleDifference)
		{
			// Calculate the Z coordinate of the other Characters lower end
			float OtherFootZ = OtherComp->Bounds.Origin.Z - OtherComp->Bounds.BoxExtent.Z;

			// The affected Character is stomped if the Hit happened at an height between the other Character's lower end and the affected Character's height higher;
			// this prevents the Character being stomped when it shouldn't, e.g. when hitting the other Character's head
			if (Hit.ImpactPoint.Z - OtherFootZ <= HitComponent->Bounds.BoxExtent.Z * 2)
			{
				AffectedCharacter->KilledBy(OtherCharacter);
			}
		}
	}
}