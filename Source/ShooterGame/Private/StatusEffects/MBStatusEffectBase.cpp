#include "ShooterGame.h"
#include "StatusEffects/MBStatusEffectBase.h"

// Empty implementations of the methods, needed to allow both native and BP overrides, but without the obligation of implementing useless functions

void UMBStatusEffectBase::OnApplied_Implementation(AShooterCharacter* AffectedCharacter) {}
void UMBStatusEffectBase::OnRemoved_Implementation(AShooterCharacter* AffectedCharacter) {}

void UMBStatusEffectBase::SimulateApplication_Implementation(AShooterCharacter* AffectedCharacter) {}
void UMBStatusEffectBase::SimulateRemoval_Implementation(AShooterCharacter* AffectedCharacter) {}
void UMBStatusEffectBase::SimulateInstant_Implementation(AShooterCharacter* AffectedCharacter) {}

bool UMBStatusEffectBase::CanStack_Implementation() { return true; }