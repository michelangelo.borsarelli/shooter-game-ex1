// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterGame.h"
#include "StatusEffects/MBFreezeEffect.h"

void UMBFreezeEffect::OnApplied_Implementation(AShooterCharacter* AffectedCharacter)
{
	// Lock gameplay actions on application
	AffectedCharacter->SetLockActions(true);
}

void UMBFreezeEffect::OnRemoved_Implementation(AShooterCharacter* AffectedCharacter)
{
	// Unlock gameplay actions on removal
	AffectedCharacter->SetLockActions(false);
}

void UMBFreezeEffect::SimulateApplication_Implementation(AShooterCharacter* AffectedCharacter)
{
	// If the Player is controlling the Character, execute a Camera Fade
	if (FadeTargetAlpha > 0 && AffectedCharacter->IsLocallyControlled())
	{
		APlayerController* Controller = Cast<APlayerController>(AffectedCharacter->GetController());
		
		if (Controller && Controller->PlayerCameraManager)
		{
			Controller->PlayerCameraManager->StartCameraFade(0.0f, FadeTargetAlpha, FadeDuration, FadeColor, false, true);
		}
	}

	// If an overlay texture has been provided, apply it to all Materials on both 1P and 3P meshes
	if (OverlayTexture)
	{
		for (int32 i = 0; i < AffectedCharacter->GetMesh()->GetNumMaterials(); i++)
		{
			UMaterialInstanceDynamic* Mat = Cast<UMaterialInstanceDynamic>(AffectedCharacter->GetMesh()->GetMaterial(i));

			if (Mat)
			{
				Mat->SetTextureParameterValue("OverlayTexture", OverlayTexture);
				Mat->SetScalarParameterValue("OverlayAmount", OverlayAmount);
			}
		}

		for (int32 i = 0; i < AffectedCharacter->GetMesh1P()->GetNumMaterials(); i++)
		{
			UMaterialInstanceDynamic* Mat = Cast<UMaterialInstanceDynamic>(AffectedCharacter->GetMesh()->GetMaterial(i));

			if (Mat)
			{
				Mat->SetTextureParameterValue("OverlayTexture", OverlayTexture);
				Mat->SetScalarParameterValue("OverlayAmount", OverlayAmount);
			}
		}
	}

	// Freeze all animations
	AffectedCharacter->GetMesh()->bPauseAnims = true;
	AffectedCharacter->GetMesh1P()->bPauseAnims = true;
}

void UMBFreezeEffect::SimulateRemoval_Implementation(AShooterCharacter* AffectedCharacter)
{
	// If the Player is controlling the Character, undo the Camera Fade
	if (FadeTargetAlpha > 0 && AffectedCharacter->IsLocallyControlled())
	{
		APlayerController* Controller = Cast<APlayerController>(AffectedCharacter->GetController());

		if (Controller && Controller->PlayerCameraManager)
		{
			Controller->PlayerCameraManager->StartCameraFade(FadeTargetAlpha, 0.0f, FadeDuration, FadeColor, false, true);
		}
	}

	// If an overlay texture was provided, zero out the overlay Amount to remove it (the texture will be eventually changed when required)
	if (OverlayTexture)
	{
		for (int32 i = 0; i < AffectedCharacter->GetMesh()->GetNumMaterials(); i++)
		{
			UMaterialInstanceDynamic* Mat = Cast<UMaterialInstanceDynamic>(AffectedCharacter->GetMesh()->GetMaterial(i));

			if (Mat)
			{
				Mat->SetScalarParameterValue("OverlayAmount", 0.0f);
			}
		}

		for (int32 i = 0; i < AffectedCharacter->GetMesh1P()->GetNumMaterials(); i++)
		{
			UMaterialInstanceDynamic* Mat = Cast<UMaterialInstanceDynamic>(AffectedCharacter->GetMesh()->GetMaterial(i));

			if (Mat)
			{
				Mat->SetScalarParameterValue("OverlayAmount", 0.0f);
			}
		}

		// Unfreeze all animations
		AffectedCharacter->GetMesh()->bPauseAnims = false;
		AffectedCharacter->GetMesh1P()->bPauseAnims = false;
	}
}