#include "ShooterGame.h"
#include "StatusEffects/MBStatusEffectHandlerComponent.h"

#pragma region FMBStatusEffectInfo

float FMBStatusEffectInfo::GetRemainingTimeSeconds(UObject* WorldContextObject)
{
	if (WorldContextObject && WorldContextObject->GetWorld())
	{
		return FMath::Max(0.0f, EndTime - WorldContextObject->GetWorld()->GetGameState()->GetServerWorldTimeSeconds());
	}

	return 0.0f;
}

float FMBStatusEffectInfo::GetDuration()
{
	return EndTime - StartTime;
}

#pragma endregion

#pragma region UMBStatusEffectHandlerComponent

// Sets default values for this component's properties
UMBStatusEffectHandlerComponent::UMBStatusEffectHandlerComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	SetIsReplicatedByDefault(true);
}

void UMBStatusEffectHandlerComponent::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	/** 
	* Replicate to ensure Clients who connect AFTER the effects are applied know about them and
	* can eventually use this information. No need to notify on replication, as it is done via
	* NetMulticast RPC.
	*/
	DOREPLIFETIME(UMBStatusEffectHandlerComponent, AppliedEffects);
}

#pragma region Callbacks

// Called when the game starts
void UMBStatusEffectHandlerComponent::BeginPlay()
{
	Super::BeginPlay();

	// Set NextId to its first value
	NextId = 0;
	OwnerCharacter = Cast<AShooterCharacter>(GetOwner());

	// Bind to Owner's death delegate
	OwnerCharacter->OnCharacterDied.AddDynamic(this, &UMBStatusEffectHandlerComponent::OnOwnerDied);

	// Late joiners should apply FX of already applied effects!
	if (GetOwner()->GetLocalRole() != NM_DedicatedServer)
	{
		for (FMBStatusEffectInfo EffectInfo : AppliedEffects)
		{
			EffectInfo.Source->SimulateApplication(OwnerCharacter);
		}

	}
}

void UMBStatusEffectHandlerComponent::OnOwnerDied(AShooterCharacter* Character)
{
	// When Owner dies, remove all Status Effects in order to avoid FX persisting after death
	if (OwnerCharacter->HasAuthority())
		ClearStatusEffects();
}

#pragma endregion


#pragma region Status Effect Management (AUTHORITY)

bool UMBStatusEffectHandlerComponent::AddStatusEffect(UMBStatusEffectBase* InEffectSource, float Duration, uint8& OutId)
{
	// Status effects can be added only with Authority (Server)
	if (OwnerCharacter->HasAuthority() && InEffectSource)
	{
		// Search for the same effect (by source) and apply the required policy
		for (int i = 0; i < AppliedEffects.Num(); i++)
		{
			if (AppliedEffects[i].Source == InEffectSource)
			{
				// Refresh the effects duration if it can be refreshed, otherwise just ignore
				if (InEffectSource->StackingPolicy == EMBStatusEffectStackingPolicy::Refresh)
				{
					// Refresh duration
					AppliedEffects[i].StartTime = GetWorld()->GetGameState()->GetServerWorldTimeSeconds();
					AppliedEffects[i].EndTime = AppliedEffects[i].StartTime + Duration;

					// Refresh Timer
					OwnerCharacter->GetWorldTimerManager().ClearTimer(AppliedEffects[i].TimerHandle);
					FTimerHandle NewHandle;
					FTimerDelegate RemoveDelegate = FTimerDelegate::CreateUObject(this, &UMBStatusEffectHandlerComponent::RemoveStatusEffect, AppliedEffects[i].Id);
					OwnerCharacter->GetWorldTimerManager().SetTimer(NewHandle, RemoveDelegate, Duration, false);
					AppliedEffects[i].TimerHandle = NewHandle;
					
					// Notify Clients that the Effect has been refresh, sending new timestamps
					Multicast_OnStatusEffectRefreshed(AppliedEffects[i].Id, AppliedEffects[i].StartTime, AppliedEffects[i].EndTime);
					OutId = AppliedEffects[i].Id;
					return true;
				}
				else if (InEffectSource->StackingPolicy == EMBStatusEffectStackingPolicy::Stack && InEffectSource->CanStack())
				{
					break;
				}
				else
				{
					return false;
				}
			}
	}
	

		if (Duration == 0)
		{
			// Effect is istantaneous, notify all with instant RPC
			Multicast_OnInstantStatusEffect(InEffectSource);
			return true;
		}

		// Add the Effect:
		// Create new Effect Info
		FMBStatusEffectInfo NewEffectInfo = FMBStatusEffectInfo();
		NewEffectInfo.Source = InEffectSource;
		NewEffectInfo.Id = GetNextId();
		NewEffectInfo.StartTime = GetWorld()->GetGameState()->GetServerWorldTimeSeconds();
		NewEffectInfo.EndTime = NewEffectInfo.StartTime + Duration;

		if (Duration > 0)
		{
			// Effect is not permanent, set a Timer to remove it
			FTimerHandle Handle;
			FTimerDelegate RemoveDelegate = FTimerDelegate::CreateUObject(this, &UMBStatusEffectHandlerComponent::RemoveStatusEffect, NewEffectInfo.Id);
			OwnerCharacter->GetWorldTimerManager().SetTimer(Handle, RemoveDelegate, Duration, false);
			NewEffectInfo.TimerHandle = Handle;
		}

		// Add to Applied Effects and notify everyone
		AppliedEffects.Add(NewEffectInfo);
		Multicast_OnStatusEffectApplied(NewEffectInfo);

		OutId = NewEffectInfo.Id;
		return true;
	}

	return false;
}

void UMBStatusEffectHandlerComponent::RemoveStatusEffect(uint8 EffectId)
{
	// Cannot remove effects without authority!
	if (OwnerCharacter->HasAuthority())
	{
		int EffectIndex = GetStatusEffectIndex(EffectId);

		if (EffectIndex > -1)
		{
			// Notify everyone of Effect removal and remove it from the array
			Multicast_OnStatusEffectRemoved(EffectId);
			AppliedEffects.RemoveAt(EffectIndex);
		}
	}

}

void UMBStatusEffectHandlerComponent::ClearStatusEffects()
{
	// Cannot remove effects without authority!
	if (OwnerCharacter->HasAuthority())
	{
		Multicast_OnStatusEffectsCleared();
		AppliedEffects.Empty();
	}
}


#pragma endregion

#pragma region RPCs

void UMBStatusEffectHandlerComponent::Multicast_OnStatusEffectApplied_Implementation(FMBStatusEffectInfo EffectInfo)
{
	// Ensure Source's WorldPtr is not nullptr
	EffectInfo.Source->WorldPtr = GetWorld();

	// With Authority (Server), execute the application callback
	if (OwnerCharacter->HasAuthority())
		EffectInfo.Source->OnApplied(OwnerCharacter);
	else
		AppliedEffects.Add(EffectInfo);

	// Eventually play FX/update UI locally
	if (OwnerCharacter->GetNetMode() != NM_DedicatedServer)
		EffectInfo.Source->SimulateApplication(OwnerCharacter);

	OnStatusEffectApplied.Broadcast(EffectInfo.Id);
}

void UMBStatusEffectHandlerComponent::Multicast_OnStatusEffectRemoved_Implementation(uint8 EffectId)
{
	FMBStatusEffectInfo EffectInfo;

	if (GetStatusEffect(EffectId, EffectInfo))
	{
		OnStatusEffectRemovedInternal(EffectInfo);
	}
}

void UMBStatusEffectHandlerComponent::Multicast_OnInstantStatusEffect_Implementation(UMBStatusEffectBase* EffectSource)
{
	// Ensure Source's WorldPtr is not nullptr
	EffectSource->WorldPtr = GetWorld();

	// Effect is instantaneous: no need to add it, just call its functions directly
	if (OwnerCharacter->HasAuthority())
	{
		EffectSource->OnApplied(OwnerCharacter);
		EffectSource->OnRemoved(OwnerCharacter);
	}

	// Eventually play FX/update UI locally
	if (OwnerCharacter->GetNetMode() != NM_DedicatedServer)
		EffectSource->SimulateInstant(OwnerCharacter);
}

void UMBStatusEffectHandlerComponent::Multicast_OnStatusEffectRefreshed_Implementation(uint8 EffectId, float NewStartTime, float NewEndTime)
{
	// Effect is instantaneous: no need to add it, just call its functions directly
	if (!OwnerCharacter->HasAuthority())
	{
		int EffectIndex = GetStatusEffectIndex(EffectId);
		AppliedEffects[EffectIndex].StartTime = NewStartTime;
		AppliedEffects[EffectIndex].EndTime = NewEndTime;
	}

	OnStatusEffectRefreshed.Broadcast(EffectId);
}

void UMBStatusEffectHandlerComponent::Multicast_OnStatusEffectsCleared_Implementation()
{
	for (FMBStatusEffectInfo EffectInfo : AppliedEffects)
	{
		OnStatusEffectRemovedInternal(EffectInfo);
	}
}

void UMBStatusEffectHandlerComponent::OnStatusEffectRemovedInternal(FMBStatusEffectInfo EffectInfo)
{
	// Ensure Source's WorldPtr is not nullptr
	EffectInfo.Source->WorldPtr = GetWorld();

	// With Authority (Server), execute the removal callback
	if (OwnerCharacter->HasAuthority())
		EffectInfo.Source->OnRemoved(OwnerCharacter);

	// Eventually play FX/update UI locally
	if (OwnerCharacter->GetNetMode() != NM_DedicatedServer)
		EffectInfo.Source->SimulateRemoval(OwnerCharacter);

	OnStatusEffectRemoved.Broadcast(EffectInfo.Id);
}

#pragma endregion

#pragma region Getters

bool UMBStatusEffectHandlerComponent::GetStatusEffect(uint8 EffectId, FMBStatusEffectInfo& OutEffectInfo)
{
	// Just get the index and use it to fetch the Status Effect Info
	int OutIndex = GetStatusEffectIndex(EffectId);

	if (OutIndex > -1)
	{
		OutEffectInfo = AppliedEffects[OutIndex];
		return true;
	}
	else
	{
		return false;
	}
}


int UMBStatusEffectHandlerComponent::GetStatusEffectIndex(uint8 EffectId)
{
	// Determine the Effect's array index and copy the Info on the out-parameter
	for (int i = 0; i < AppliedEffects.Num(); i++)
	{
		if (AppliedEffects[i].Id == EffectId)
		{
			return i;
		}
	}

	return -1;
}

#pragma endregion

#pragma endregion
