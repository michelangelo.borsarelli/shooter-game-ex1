// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/VerticalBox.h"
#include "UI/UserWidgets/MBStatusEffectTracker.h"
#include "StatusEffects/MBStatusEffectHandlerComponent.h"
#include "MBStatusEffectsList.generated.h"

/**
 * Auto-updated list of Status Effects applied to the local Player's Character.
 */
UCLASS(meta = (BlueprintSpawnableComponent))
class SHOOTERGAME_API UMBStatusEffectsList : public UUserWidget
{
	GENERATED_BODY()

#pragma region FUNCTIONS

protected:
	virtual void NativeConstruct() override;

	/** Called when the local PlayerController changes Pawn */
	UFUNCTION()
		void OnPawnChange(APawn* NewPawn);

	/** Called when a Status Effect is applied on the tracked Handler */
	UFUNCTION()
		void OnStatusEffectApplied(uint8 Id);

	/** Called when a Status Effect is removed from the tracked Handler */
	UFUNCTION()
		void OnStatusEffectRemoved(uint8 Id);


private:
	/** Gets a collapsed Tracker if available, otherwise creates a new one */
	UMBStatusEffectTracker* GetTrackerInstance();

#pragma endregion

#pragma region PROPERTIES

public:
	/** Main container wrapping the individual Status Effect Trackers */
	UPROPERTY(meta=(BindWidget))
	UVerticalBox* MainBox;

	/** Status Effect Tracker class to use */
	UPROPERTY(EditAnywhere)
	TSubclassOf<UMBStatusEffectTracker> TrackerClass;

	/** Vertical spacing between Status Effect Trackers in the list */
	UPROPERTY(EditAnywhere)
	float Spacing;


private:
	/** Currently tracked Status Effect Handler */
	UMBStatusEffectHandlerComponent* TrackedEffectHandler;

#pragma endregion

};
