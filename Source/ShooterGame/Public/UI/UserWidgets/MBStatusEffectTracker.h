#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "Components/ProgressBar.h"
#include "StatusEffects/MBStatusEffectHandlerComponent.h"
#include "MBStatusEffectTracker.generated.h"

/**
 * User Widget which can be used to keep track of the state of a Status Effect.
 */
UCLASS(Abstract)
class SHOOTERGAME_API UMBStatusEffectTracker : public UUserWidget
{
	GENERATED_BODY()

#pragma region FUNCTIONS

protected:
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	/** Updates the Widget based on last cached Status Effect Info */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void UpdateFromStatusEffectInfo();
	virtual void UpdateFromStatusEffectInfo_Implementation();

	/** Formats the provided time value according to the Widget's settings. */
	UFUNCTION(BlueprintCallable)
	void SetRemainingTimeText(float RemainingTime);

public:
	/** Shows the Widget; can be overridden in Blueprint to define custom behaviour */
	UFUNCTION(BlueprintNativeEvent)
	void Show();
	virtual void Show_Implementation();

	/** Hides the Widget; can be overridden in Blueprint to define custom behaviour */
	UFUNCTION(BlueprintNativeEvent)
	void Collapse();
	virtual void Collapse_Implementation();

	/** Prepares the Widget, identifying the bound Status Effect via its Id and its Handler */
	UFUNCTION(BlueprintCallable)
	void Setup(UMBStatusEffectHandlerComponent* InHandler, uint8 InEffectId);

	/** Sets the Tracker's Icon, based on UMG design */
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void SetStatusEffectIcon(UTexture2D* InIcon);

	/** Returns the Status Effect Id to which the Tracker is bound */
	UFUNCTION(BlueprintCallable, BlueprintPure)
	uint8 GetStatusEffectId() { return StatusEffectId; }

	/** Returns the Status Effect Handler to which the Tracker is bound */
	UFUNCTION(BlueprintCallable, BlueprintPure)
	UMBStatusEffectHandlerComponent* GetReferenceHandler() { return ReferenceHandler; }

#pragma endregion

#pragma region PROPERTIES

public:
	/** Optional text showing remaining duration of the Status Effect */
	UPROPERTY(BlueprintReadOnly, meta = (BindWidgetOptional))
	UTextBlock* RemainingTimeText;

	/** Optional progress bar showing the current duration percent of the Status Effect */
	UPROPERTY(BlueprintReadOnly, meta = (BindWidgetOptional))
	UProgressBar* RemainingTimeBar;

	/** Decimal precision of the Remaining Time Text */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int TextPrecision;

protected:
	/** Cached Status Effect Info */
	UPROPERTY(BlueprintReadWrite)
	FMBStatusEffectInfo StatusEffectInfo;

private:
	/** Bound Status Effect Id */
	uint8 StatusEffectId;

	/** Bound Status Effect Handler */
	UMBStatusEffectHandlerComponent* ReferenceHandler;

#pragma endregion
};
