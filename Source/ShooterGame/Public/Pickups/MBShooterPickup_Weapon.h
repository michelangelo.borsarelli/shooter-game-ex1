#pragma once

#include "CoreMinimal.h"
#include "Pickups/ShooterPickup.h"
#include "Weapons/ShooterWeapon.h"
#include "MBShooterPickup_Weapon.generated.h"

/**
* Struct representing the visual data of the Weapon Pickup.
* Pointers stored in the struct should always point to assets in order to be replicated.
*/
USTRUCT()
struct FMBWeaponGraphics
{
	GENERATED_BODY()

	/** Skeletal Mesh of the Weapon */
	UPROPERTY()
	USkeletalMesh* Mesh;

	/** Materials to apply on the Skeletal Mesh; Dynamic Material Instances are not supported */
	UPROPERTY()
	TArray<UMaterialInterface*> Materials;
};

/**
 * Pickup object that grants Players a new Weapon, replacing their current one.
 * If the Player already owns the Weapon in their inventory, then this will add ammo to that instead.
 */
UCLASS()
class SHOOTERGAME_API AMBShooterPickup_Weapon : public AShooterPickup
{
	GENERATED_BODY()

public:
	AMBShooterPickup_Weapon(const FObjectInitializer& ObjectInitializer);

	virtual void Tick(float DeltaSeconds) override;

	/** Sets up the Pickup based on an already existing Weapon, copying its Class, Ammo and graphic data */
	UFUNCTION(BlueprintCallable)
	void SetupFromWeapon(AShooterWeapon* SourceWeapon);

protected:
	virtual void BeginPlay() override;

	// Pickup overrides
	virtual bool CanBePickedUp(class AShooterCharacter* TestPawn) const override;
	
	virtual void GivePickupTo(class AShooterCharacter* Pawn) override;
	
	virtual void OnPickedUp() override;
	virtual void OnRespawned() override;
	virtual void OnExpired() override;
	
	// Graphics functions
	UFUNCTION()
	void OnRep_GraphicData();

	/** Updates the Pickup's Mesh and Materials based on received Graphic Data */
	virtual void UpdateGraphics();

protected:
	/** Skeletal Mesh representing the granted Weapon */
	UPROPERTY(VisibleAnywhere, Category = Mesh)
	USkeletalMeshComponent* WeaponMesh;

	/** Root of the Weapon Mesh, used for movement purposes */
	UPROPERTY(VisibleAnywhere, Category = Pickup)
	USceneComponent* MeshRoot;
	
	/** Class of the Weapon to grant */
	UPROPERTY(EditAnywhere, Category = Weapon)
	TSubclassOf<AShooterWeapon> WeaponClass;

	/** Starting ammo of the granted Weapon */
	UPROPERTY(EditAnywhere, Category = Weapon)
	int32 Ammo;
	
	/** Angular speed at which the Mesh rotates around Z-axis */
	UPROPERTY(EditAnywhere, Category = Pickup)
	float RotationSpeed;

	/** Replicated Graphic Data, used to update the Pickup's Mesh on Clients */
	UPROPERTY(Transient, ReplicatedUsing = OnRep_GraphicData)
	FMBWeaponGraphics GraphicData;

};
