#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Player/ShooterCharacter.h"
#include "MBStatusEffectBase.generated.h"

UENUM(BlueprintType)
enum class EMBStatusEffectStackingPolicy : uint8
{
	Ignore = 0 UMETA(DisplayName = "Ignore"),				// Do not stack, ignore subsequent applications
	Refresh = 1 UMETA(DisplayName = "Refresh duration"),	// Do not stack, refresh duration of the effect
	Stack = 2 UMETA(DisplayName = "Stack")					// Stack, apply another copy of the effect
};


/** 
* Base class for Status Effects.
* This class exposes basic callbacks for application and removal resolution.
* The state of the Character should be changed in OnApplied and OnRemoved, which are called on Server only.
* Simulate|...| methods are instead used to play FX locally on Clients.
* 
* As Data Assets, Status Effects can be instanced as assets in the Editor, but contain runtime logic that
* is used to execute the actual effect.
*/
UCLASS(Abstract)
class SHOOTERGAME_API UMBStatusEffectBase : public UDataAsset
{
	GENERATED_BODY()
		
#pragma region FUNCTIONS

public:
	/** 
	* GetWorld is overridden in order to allow Blueprints to access World nodes (e.g. Spawn, Timer, ...) 
	* The World pointer is populated externally at runtime when needed.
	*/
	virtual class UWorld* GetWorld() const override { return WorldPtr; }

	/** The Status Effect is being applied, called to change the Character's state on Server */
	UFUNCTION(BlueprintNativeEvent)
	void OnApplied(AShooterCharacter* AffectedCharacter);

	/** The Status Effect is being removed, called to change the Character's state on Server */
	UFUNCTION(BlueprintNativeEvent)
	void OnRemoved(AShooterCharacter* AffectedCharacter);

	/** Locally play FX on Clients on application, Character's state should be changed in OnApplied */
	UFUNCTION(BlueprintNativeEvent)
	void SimulateApplication(AShooterCharacter* AffectedCharacter);

	/** Locally play FX on Clients on removal, Character's state should be changed in OnRemoved  */
	UFUNCTION(BlueprintNativeEvent)
	void SimulateRemoval(AShooterCharacter* AffectedCharacter);

	/** Locally play FX on Clients when the effect is instantaneous, Character's state should be changed in OnApplied / OnRemoved  */
	UFUNCTION(BlueprintNativeEvent)
	void SimulateInstant(AShooterCharacter* AffectedCharacter);

	/** Regardless the customizable policy, can this type of Effect actually stack? */
	UFUNCTION(BlueprintNativeEvent)
	bool CanStack();

#pragma endregion

#pragma region PROPERTIES

public:
	/** How this Effect should behave when it is applied twice on the same Character */
	UPROPERTY(EditAnywhere, Category="Base Settings")
	EMBStatusEffectStackingPolicy StackingPolicy;

	/** 2D Icon representing the Status Effect. */
	UPROPERTY(EditAnywhere, Category = "Visual")
	UTexture2D* Icon;

	/** Pointer to current world, populated externally */
	UWorld* WorldPtr;

#pragma endregion
};
