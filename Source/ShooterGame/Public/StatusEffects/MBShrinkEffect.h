// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "StatusEffects/MBStatusEffectBase.h"
#include "MBShrinkEffect.generated.h"

/**
 * On application, this Status Effect changes the scale of the affected Character.
 * If then another Character manages to step onto them, they are killed instantly.
 * 
 * This Effect cannot stack.
 */
UCLASS()
class SHOOTERGAME_API UMBShrinkEffect : public UMBStatusEffectBase
{
	GENERATED_BODY()
	
#pragma region FUNCTIONS

protected:
	void OnApplied_Implementation(AShooterCharacter* AffectedCharacter) override;
	void OnRemoved_Implementation(AShooterCharacter* AffectedCharacter) override;

	/** Called when an affected Character's capsule receives a hit */
	UFUNCTION()
	void OnCharacterHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	
	// This effect cannot stack
	bool CanStack_Implementation() override { return false; }

#pragma endregion

public:
	/** Target ScaleFactor that will be applied to the affected Character */
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Scaling)
	float TargetScaleFactor;

	/** Minimum required Scale Factor difference between Character in order to trigger the instant kill effect. */
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Scaling, meta = (ClampMin = 0.0f))
	float MinScaleDifference;

	/** Duration of the ScaleFactor interpolation */
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Scaling)
	float InterpolationTimeSeconds;

	/** 
	* Optional curve to be used by the interpolation.
	* It is considered normalized, i.e. only the [0,1] time range is considered and its value should start from 0 and end to 1.
	*/
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Scaling)
	UCurveFloat* InterpolationCurve;
};
