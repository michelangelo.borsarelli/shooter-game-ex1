#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "StatusEffects/MBStatusEffectBase.h"
#include "MBStatusEffectHandlerComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FStatusEffectDelegate, uint8, UniqueId);

/**
* Represents the replicated information about an applied Status Effect.
*/
USTRUCT(BlueprintType)
struct FMBStatusEffectInfo 
{
	GENERATED_BODY()

public:
	float GetRemainingTimeSeconds(UObject* WorldContextObject);
	float GetDuration();
public:
	/** 
	* Unique Id assigned to the Status Effect.
	* Using the uint8 type makes the property Blueprint compatible.
	* It is not expected to need more than 256 Unique Ids, considering wrapping.
	*/
	UPROPERTY(BlueprintReadOnly)
	uint8 Id;

	/** Data Asset source of the Status Effect */
	UPROPERTY(BlueprintReadOnly)
	UMBStatusEffectBase* Source;
	
	/** Application timestamp */
	UPROPERTY(BlueprintReadOnly)
	float StartTime;

	/** Removal timestamp */
	UPROPERTY(BlueprintReadOnly)
	float EndTime;

	/** If the effect is not unlimited, handle for the removal timer (Server only, not replicated) */
	FTimerHandle TimerHandle;
};


/**
* ActorComponent responsible of handling Status Effects.
* It should only be attached to a Ext Shooter Character.
*/
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHOOTERGAME_API UMBStatusEffectHandlerComponent : public UActorComponent
{
	GENERATED_BODY()

#pragma region FUNCTIONS

public:	
	UMBStatusEffectHandlerComponent();

	/** Adds a Status Effect based on the provided Data Asset source. 
	*
	* @param EffectSource Source Data Asset
	* @param Duration Duration of the Status Effect (0 -> Instant, -1 -> Unlimited)
	* @param OutId Id assigned to the applied added Effect, if successful (out parameter)
	* 
	* @return True if the Effect was added successfully
	*/
	UFUNCTION(BlueprintCallable)
	bool AddStatusEffect(UMBStatusEffectBase* EffectSource, float Duration, uint8& OutId);

	/** Removes the Status Effect identified by the provided Id. */
	UFUNCTION(BlueprintCallable)
	void RemoveStatusEffect(uint8 EffectId);

	/** Removes all Status Effects. */
	UFUNCTION(BlueprintCallable)
	void ClearStatusEffects();

	/** 
	* Gets the Status Effect Info corresponding to the provided Id and stores it into OutEffectInfo. 
	* 
	* @return True if the Status Effect was found.
	*/
	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool GetStatusEffect(uint8 EffectId, FMBStatusEffectInfo& OutEffectInfo);

	/**
	* Finds the AppliedEffects Index corresponding to the Status Effect with the provided Id.
	* 
	* @returns Index if found, -1 if not found.
	*/
	UFUNCTION(BlueprintCallable, BlueprintPure)
	int GetStatusEffectIndex(uint8 EffectId);

private:
	/** 
	* Get the next ID. 
	* In case of overflow, uint8 wraps around to zero; it is not expected to ever need more than 255 unique IDs at the same time. 
	*/
	uint8 GetNextId() { return NextId++; }

	/** Internally shared Status Effect removal logic. */
	void OnStatusEffectRemovedInternal(FMBStatusEffectInfo EffectInfo);

protected:
	virtual void BeginPlay() override;

	/** Executes application logic on Server, notifies Clients */
	UFUNCTION(NetMulticast, reliable)
	void Multicast_OnStatusEffectApplied(FMBStatusEffectInfo EffectInfo);
	virtual void Multicast_OnStatusEffectApplied_Implementation(FMBStatusEffectInfo EffectInfo);

	/** Executes removal logic on Server, notifies Clients */
	UFUNCTION(NetMulticast, reliable)
	void Multicast_OnStatusEffectRemoved(uint8 EffectId);
	virtual void Multicast_OnStatusEffectRemoved_Implementation(uint8 EffectId);


	/** Executes instant logic on Server, notifies Clients */
	UFUNCTION(NetMulticast, reliable)
	void Multicast_OnInstantStatusEffect(UMBStatusEffectBase* EffectSource);
	virtual void Multicast_OnInstantStatusEffect_Implementation(UMBStatusEffectBase* EffectSource);

	/** Notifies Clients of the Status Effect's refreshment. */
	UFUNCTION(NetMulticast, reliable)
	void Multicast_OnStatusEffectRefreshed(uint8 EffectId, float NewStartTime, float NewEndTime);
	virtual void Multicast_OnStatusEffectRefreshed_Implementation(uint8 EffectId, float NewStartTime, float NewEndTime);

	/** Executes removal logic on Server, notifies Clients */
	UFUNCTION(NetMulticast, reliable)
	void Multicast_OnStatusEffectsCleared();
	virtual void Multicast_OnStatusEffectsCleared_Implementation();

	/** Called when the Owner Shooter Character dies. */
	UFUNCTION()
	void OnOwnerDied(AShooterCharacter* Character);

#pragma endregion


#pragma region PROPERTIES

public:
	/** Locally broadcast when notified of Status Effect application. */
	UPROPERTY(BlueprintAssignable)
	FStatusEffectDelegate OnStatusEffectApplied;

	/** Locally broadcast when notified of Status Effect removal. */
	UPROPERTY(BlueprintAssignable)
	FStatusEffectDelegate OnStatusEffectRemoved;

	/** Locally broadcast when notified of Status Effect refreshment. */
	UPROPERTY(BlueprintAssignable)
	FStatusEffectDelegate OnStatusEffectRefreshed;


	/** Currently applied Effects */
	UPROPERTY(Replicated)
	TArray<FMBStatusEffectInfo> AppliedEffects;

protected:
	/** Owner of this component, already cast to Ext Shooter Character. */
	UPROPERTY(BlueprintReadOnly)
	AShooterCharacter* OwnerCharacter;
	
private:
	uint8 NextId;

#pragma endregion

};
