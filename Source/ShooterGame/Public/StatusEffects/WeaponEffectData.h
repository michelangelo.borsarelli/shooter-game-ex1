// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "StatusEffects/MBStatusEffectBase.h"
#include "WeaponEffectData.generated.h"

/**
* Represents a Status Effect that can be applied by a Weapon.
*/
USTRUCT()
struct FMBWeaponEffectData
{
	GENERATED_BODY()

public:
	/** Status Effect source Data Asset */
	UPROPERTY(EditDefaultsOnly, Category="Weapon Effect")
	UMBStatusEffectBase* StatusEffect;

	/** 
	* Duration of the Effect when applied (in seconds)
	* 
	* If set to 0, the Effect will be considered instantaneous.
	* If set to a negative value, the Effect will be considered permanent.
	*/
	UPROPERTY(EditDefaultsOnly, Category = "Weapon Effect")
	float Duration;

	/** Chance of the Effect being applied (can be used by Weapons to determine whether to apply the Effect or not) */
	UPROPERTY(EditDefaultsOnly, Category = "Weapon Effect", meta = (ClampMin = "0.0", ClampMax = "1.0", UIMin = "0.0", UIMax = "1.0"))
	float Chance;
};
