// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "StatusEffects/MBStatusEffectBase.h"
#include "MBFreezeEffect.generated.h"

/**
 * On application, this Status Effect locks the affected Character's Gameplay Actions till it's removed.
 * 
 * Additionally, a Camera Fade and an overlay Texture on the Character's Mesh are applied for visual feedback.
 * 
 * This Effect cannot stack.
 */
UCLASS()
class SHOOTERGAME_API UMBFreezeEffect : public UMBStatusEffectBase
{
	GENERATED_BODY()
	
#pragma region FUNCTIONS

	void OnApplied_Implementation(AShooterCharacter* AffectedCharacter) override;
	void OnRemoved_Implementation(AShooterCharacter* AffectedCharacter) override;

	void SimulateApplication_Implementation(AShooterCharacter* AffectedCharacter) override;
	void SimulateRemoval_Implementation(AShooterCharacter* AffectedCharacter) override;

	// This Effect cannot stack
	bool CanStack_Implementation() override { return false; }
#pragma endregion

#pragma region PROPERTIES

	/** Color of the Camera fade when applied */
	UPROPERTY(EditAnywhere, Category = FX)
	FLinearColor FadeColor;

	/** Duration of the Camera fade when applied */
	UPROPERTY(EditAnywhere, Category = FX)
	float FadeDuration;

	/** target Alpha value of the Camera fade when applied */
	UPROPERTY(EditAnywhere, Category = FX)
	float FadeTargetAlpha;

	/** Optional overlay Texture to apply on hit target's mesh */
	UPROPERTY(EditAnywhere, Category = FX)
	UTexture2D* OverlayTexture;

	/** How much should be the overlay Texture visible? */
	UPROPERTY(EditAnywhere, Category = FX)
	float OverlayAmount;

#pragma endregion
};
