#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Player/ShooterCharacter.h"
#include "MBInterpolatorComponent.generated.h"

/**
* Struct representing a generic interpolation, handling the timing aspect.
*/
USTRUCT()
struct FMBInterpolation
{
	GENERATED_BODY()
	
	friend class UMBInterpolatorComponent;

public:
	bool IsComplete() const { return CurrentTime >= Duration; }
	float GetPercent() const { return CurrentTime / Duration; }

protected:
	/** 
	* Makes the interpolation tick
	* 
	* @return True if the Tick was successful
	*/
	bool TickInterpolation(float DeltaSeconds);
	
private:
	/**
	* To be implemented by child structs, called during Interpolation Tick;
	* it is expected to perform the actual interpolation
	* 
	* @return True if the Tick was successful
	*/
	virtual bool OnTick(float DeltaSeconds) PURE_VIRTUAL(FMBInterpolation::OnTick, return true;);

protected:
	/** Server Timestamp (seconds) at which the interpolation was started */
	UPROPERTY()
	float StartTimestamp;

	/** Current time of the interpolation */
	float CurrentTime;

	/** Duration of the interpolation */
	float Duration;
};



/** 
* A custom Interpolation struct for interpolating the value of a Character's Scale Factor.
*/
USTRUCT()
struct FMBScaleFactorInterpolation : public FMBInterpolation
{
	GENERATED_BODY()

	friend class UMBInterpolatorComponent;

protected:
	/** Target Character of the interpolation */
	UPROPERTY()
	AShooterCharacter* TargetCharacter;

	/** 
	* Optional Curve to be used for the interpolation.
	* It is considered normalized, i.e. only the [0,1] time range is considered
	* and its value should start from 0 and end to 1 (unless a different behaviour
	* is desired), as 0 represents StartVal and 1 represents EndVal
	*/
	UPROPERTY()
	UCurveFloat* Curve;

	/** Starting ScaleFactor value. */
	UPROPERTY()
	float StartVal;
	
	/** Target ScaleFactor value. */
	UPROPERTY()
	float EndVal;

	UPROPERTY()
	/** If true, the Curve will be inverted (reflection around (0.5, 0.5)) */
	bool bInverseCurve;

private:
	virtual bool OnTick(float DeltaSeconds) override;
};




/**
* This ActorComponent manages interpolations relating to its Owner (generally a ShooterCharacter).
*/
UCLASS()
class SHOOTERGAME_API UMBInterpolatorComponent : public UActorComponent
{
	GENERATED_BODY()
	
public:	
	UMBInterpolatorComponent();

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	/** 
	* Interpolates the ScaleFactor of the Owner if it is a ShooterCharacter.
	* 
	* @param InStartVal Starting value
	* @param InEndVal Target value
	* @param InDurationSeconds Desired duration
	* @param InCurve Optional Curve to be used with the interpolation, it is considered normalized, i.e. only the [0,1] time range is considered
	*		 and its value should start from 0 and end to 1
	* @param bUseInverseCurve If true, reflects the curve around (0.5, 0.5)
	* @param bForceStartVal If true, InStartVal will be forced instead of starting from the current value
	* @param bAdjustTime If true and bForceStartVal is false, the duration will be adjusted based on the actual starting value
	*/
	UFUNCTION(BlueprintCallable)
	void InterpolateScaleFactor(float InStartVal, float InEndVal, float InDurationSeconds, UCurveFloat* InCurve, bool bUseInverseCurve = false, bool bForceStartVal = false, bool bAdjustTime = true);

protected:
	/** Forwards a ScaleFactor interpolation request to the Server */
	UFUNCTION(Server, reliable)
	void Server_InterpolateScaleFactor(float InStartVal, float InEndVal, float InTimeSeconds, UCurveFloat* InCurve, bool bForceStartVal, bool bAdjustTime);
	virtual void Server_InterpolateScaleFactor_Implementation(float InStartVal, float InEndVal, float InTimeSeconds, UCurveFloat* InCurve, bool bForceStartVal, bool bAdjustTime);
	
	/** Last ScaleFactor interpolation set; since only one is admitted at a time, it is stored in this dedicated property */
	UPROPERTY(Transient)
	FMBScaleFactorInterpolation ScaleFactorInterpolation;
};
