// Copyright Epic Games, Inc. All Rights Reserved.

/**
 * Movement component meant for use with Pawns.
 */

#pragma once
#include "ShooterCharacterMovement.generated.h"

 /**
 * Extended Move class that is used in place of the base FSavedMove_Character used by the Character's replication system.
 * Ontop of basic Character Move info, it tracks Teleport moves as well.
 */
class SHOOTERGAME_API FMBSavedMove_ExtCharacter : public FSavedMove_Character
{
	/** If true, the Player requested a Teleport move */
	bool bPressedTeleport;

	virtual uint8 GetCompressedFlags() const override;
	virtual void SetInitialPosition(ACharacter* C) override;
};


/**
* Extended Client Prediction Data class to be used by the Extended Shooter Character Movement Component.
* It allocates FMBSavedMove_ExtCharacter moves instead of the basic Character moves.
*/
class SHOOTERGAME_API FMBNetworkPredictionData_Client_ExtCharacter : public FNetworkPredictionData_Client_Character
{
	virtual FSavedMovePtr AllocateNewMove() override;

public:
	FMBNetworkPredictionData_Client_ExtCharacter(const UCharacterMovementComponent& ClientMovement)
		: FNetworkPredictionData_Client_Character(ClientMovement)
	{
	}
};


UCLASS()
class UShooterCharacterMovement : public UCharacterMovementComponent
{
	GENERATED_UCLASS_BODY()

	virtual float GetMaxSpeed() const override;

#pragma region EXTENSION

	virtual void SetUpdatedComponent(USceneComponent* NewUpdatedComponent) override;

	virtual class FNetworkPredictionData_Client* GetPredictionData_Client() const override;
	virtual void UpdateFromCompressedFlags(uint8 Flags) override;

	// Use the OnMovementUpdated callback to actually perform moves, as it exists as an entry point for child classes
	virtual void OnMovementUpdated(float DeltaSeconds, const FVector& OldLocation, const FVector& OldVelocity) override;


#pragma region Teleport

public:
	/** Performs the Teleport */
	virtual bool DoTeleportForward(bool bReplayingMoves);

	/** Distance in forward direction at which the Teleport ability moves the Character */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Teleport)
		float TeleportDistance;

	/**
	* Maximum number of sweeps required to determine the actual Teleport destination while avoiding compenetrations
	* with Level geometry and other objects.
	*
	* If the desired destination is not free, i.e. it would create a non-adjustable compenetration, another check is
	* performed at decreased distance till a free destination is found.
	* The maximum number of sweeps determines the granularity of this process: an higher numbers checks means higher
	* precision but also a computationally heavier procedure.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Teleport)
		int TeleportMaxSweepChecks;

	/**
	* Scale factor applied to the Character's Capsule shape during Teleport sweep checks.
	*
	* The Teleport operation can adjust partial compenetrations, therefore there's no need for exact shape sweeps which
	* would hit even slight bumps on the floor, with an unsatisfying result for the Player. Reducing the scale factor
	* allows for an higher tolerance for adjustable compenetrations.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Teleport, meta = (ClampMin = "0.0", ClampMax = "1.0", UIMin = "0.0", UIMax = "1.0"))
		float TeleportCapsuleSweepScaleFactor;

#pragma endregion


#pragma region Wall Jump

public:
	/** Is this Movement Component in a state that allows Wall Jumps? */
	virtual bool CanAttemptWallJump() const;

	/**
	 * Perform Wall Jump. Called by Shooter Character when a jump has been detected because Character->bPressedJump was true but a normal Jump wasn't performed.
	 * 
	 * @param	bReplayingMoves: true if this is being done as part of replaying moves on a locally controlled client after a server correction.
	 * @return	True if the Wall Jump was triggered successfully.
	 */
	virtual bool DoWallJump(bool bReplayingMoves);
	

	/** Thickness of the outer shell added to the Character's capsule radius when checking for walls */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Wall Jump")
		float WallJumpSweepOuterShell;

	/** Minimum speed applied in the direction of the wall's normal when triggering a Wall Jump */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Wall Jump")
		float WallJumpMinNormalPushSpeed;

	/** Minimum vertical speed applied when triggering a Wall Jump */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Wall Jump")
		float WallJumpMinVerticalPushSpeed;

	/** Channel used for sweeping in search of nearby walls */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Wall Jump")
		TEnumAsByte<ECollisionChannel> WallChannel;

#pragma endregion
};

