# Shooter Game Ex1

This is the repository for the project of my exercise based on the Shooter Game template from Epic Games.
I wrote this short README to give an insight into my thought process during the implementation of the required features.

I decided to implement the following features:


- Teleport
- Wall jump
- Freezing Gun
- Shrink Gun
- Drop Weapon



**Premise**

At the beginning of my work on this exercise I thought I could implement most of the features as extensions of the ShooterGame classes, leaving those mostly intact.

Later on I realized it was mostly counterproductive, as for many things I had to change how few of ShooterGame's classes work internally. In the end, I moved my extension code into the original classes (mostly _AShooterCharacter_ and _AShooterCharacterMovement_) and removed the ones I created originally.


Other classes I created have an _MB_ prefix to make it easier to distinguish them from ShooterGame's ones.
Whenever I added code or changed the existing one in a ShooterGame's class, I tried to make it clear either by comments or by wrapping it into an EXTENSION region with _#pragma region_.


I realize that many of the things of I implemented could be furthermore refined and expanded upon, but I preferred avoiding taking too much time to complete the required features for the sake of perfection.



**Teleport**

As requested, I implemented the Wall Jump as new Movement Ability. I did not add any special FX, but I tried to keep the code open to accomodate for some. In general, I considered the Teleport as an 'instantaneous' action, in the sense that whether it is successful or not, the Player's input is consumed and they will need to release and press _T_ to try again.




**Wall jump**

Though initially not considering its implementation, while I was reviewing the Teleport's code I realized the Wall jump could be quickly implemented by appending few extra checks after the basic Jump code in _CheckJumpInput_ in _AShooterCharacter_.
My implementation considers the possibility of chained Wall jumps by resetting the Jump count every time the Character Wall Jumps and partially preserves the incoming momentum, making sure the push upwards and away from the wall is not below a minimum threshold.

I also tried to make sure Characters can't Wall jump on invisibile walls like outer Map bounds, but I may have missed few of them.


**Freezing Gun and Shrink Gun**

While I was I thinking about how to implement these two modifiers I got to a straightforward conclusion: they both are Weapons that apply a status effect to Characters which, once it's applied, lives independently from the Weapon that caused it.

I then decided to build a simple Status Effect handling system to attach to Characters as an Actor Component. Initially I made Status Effects as Data Assets with the idea of taking advantage of the possibility of making them Instanced directly inside the Weapon Blueprints. Later on I realized how big of an hassle they were without a properly setup replication channel, so I rolled back to the original version using actual assets with internal functionality - but no internal state, as it would be shared between Status Effects of the same type.

In the end I decided to call it day as it was doing its job - after all this is a system that could be improved almost indefinitely. While prepairing the final build and recording videos I thought that at this state, **Actor Components** would probably have been more fitting for the job of Status Effects, therefore I'm going to shortly explain how I would improve this if I had to expand on this system.

- Actor Components would be directly instanced and replicated, therefore they could hold an non-shared internal state and be more strictly bound to the affected Character;
- Actor Components can natively replicate, therefore there would be less need to rely on RPCs;
- Despite that, they would still support RPCs without the need of using external systems to send messages between Server and Clients;
- Instead of completely removing / destroying the Components, I would deactivate them when no more useful and reactivate them with new data / state, if that Component class was needed again;
- Moreover, they would support UE4's framework callback, like _Activate_, _Deactivate_, _TickComponent_,...;
- Instead of using uint8 IDs, I would use directly replicated pointers for easier referencing;
- A _Status Effect Handler Component_ would be still needed to manage the big picture.


Besides this common part, **Freeze** and **Shrink** effects are implemented by applying a change on Characters on application and reverting it on removal. I expanded on the _Instant_ type of Weapon by adding the possibility of giving them on-hit effects; this approach could be easily expanded to _Projectile_ Weapons by passing the list of Effects onto the projectiles themselves and applying them to any hit Character.




**Drop weapon**

To implement this feature I modified the _AShooterPickup_ class to allow for both manually placed Pickups in the Editor and runtime spawned ones, making their ability to respawn optional and adding the possibility of giving them a limited lifetime.

In particular, Weapon Pickups replace the Character's current Weapon with the one contained inside; if the Character already owns that Weapon, its ammo will be added to it instead.

This way I could both implement the Weapon drop on Character death and Map Weapon pickups to test new Weapons more easily. I made sure to place few Freeze Gun and Shrink Gun Pickups around in Sanctuary and Highrise.
